<?php

/**
 * @file
 * Module file for webform_required_message.
 */

/**
 * Implements hook_form_webform_component_edit_form_alter().
 */
function webform_required_message_form_webform_component_edit_form_alter(&$form, &$form_state, $form_id) {
  list(, $component) = $form_state['build_info']['args'];
  if (in_array($component['type'], variable_get('webform_required_message_disabled_types', array()))) {
    return;
  }
  $form['validation']['required_message'] = array(
    '#title' => t('Custom Required Message'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="required"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => isset($component['extra']['required_message']) ? $component['extra']['required_message'] : '',
  );
}

/**
 * Implements hook_webform_component_presave().
 */
function webform_required_message_webform_component_presave(&$component) {
  if (!empty($component['validation']['required_message'])) {
    $component['extra']['required_message'] = $component['validation']['required_message'];
  }
}

/**
 * Implements hook_form_alter().
 */
function webform_required_message_form_alter(&$form, $form_state, $form_id) {
  // Alter all webforms and add some custom validation.
  if (strpos($form_id, 'webform_client_form_') === 0) {
    $form['#validate'][] = 'webform_required_message_update_form_error';
  }
}

/**
 * Webform validate callback which replaces error messages.
 */
function webform_required_message_update_form_error($form, &$form_state) {
  if (empty($_SESSION['messages']['error']) || empty($form['#node']->webform['components'])) {
    return;
  }
  $errors = &$_SESSION['messages']['error'];
  foreach ($form['#node']->webform['components'] as $component) {
    if (empty($component['extra']['required_message'])) {
      continue;
    }
    // There is no way to hook into the required validation, instead check for
    // the error message in the list of errors and replace it with our own.
    $existing_error_message = t('!name field is required.', array('!name' => $component['name']));
    $replace_key = array_search($existing_error_message, $errors);
    if ($replace_key !== FALSE) {
      $errors[$replace_key] = filter_xss($component['extra']['required_message']);
    }
  }
}
